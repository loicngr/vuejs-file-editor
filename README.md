# vuejs-file-editor


![](https://i.imgur.com/HwGx736.gif)

- [Demo](https://i.imgur.com/HwGx736.gifv)


## Supported File :
- > php
- > javascript
- > markdown
- > python
- > text
- > html
- > rust 
- > json


## Macro keys :
- > (Download file) CTRL + S
- > And all VS CODE KeyWords is ready to use!

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
